package main 

import(
	"database/sql"
    "log"
    
	_ "github.com/go-sql-driver/mysql"
)

type dbQuery struct{

}

func (d *dbQuery) AddBewertung(b Bewertung,db *sql.DB){

	// Prepare statement for inserting data
    stmtIns, err := db.Prepare("INSERT INTO Bewertung VALUES( ?, ?, ? )") // ? = placeholder
    if err != nil {
        panic(err.Error()) // proper error handling instead of panic in your app
    }
    defer stmtIns.Close() 

    stmtIns.Exec(b.Nickname,b.Bewertung,b.Geruchsid)

}

func (d* dbQuery) GetAllBewertungen(db *sql.DB) Bewertungen{
	rows, err := db.Query("Select * from Bewertung;")
	if err != nil {
        log.Fatal(err.Error()) // proper error handling instead of panic in your app
    }
    defer rows.Close() 
    bewertungen := Bewertungen{}
    for rows.Next() {
    	bew := Bewertung{}
    	rows.Scan(&bew.Nickname,&bew.Bewertung,&bew.Geruchsid)
    	
    	bewertungen.Bewertungen = append(bewertungen.Bewertungen,bew)

    }

    return bewertungen



    
}

func checkCount(rows *sql.Rows)(count int){
	for rows.Next(){
		err := rows.Scan(&count)
		if err != nil{
			log.Fatal(err)
		}
	}

	return count
}