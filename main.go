package main
import(

	
	"os"
	"log"
	"io/ioutil"
	"fmt"
	"encoding/json"
	"net/http"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	//"github.com/gorilla/context"
	"strings"

	


	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"
	"github.com/dgrijalva/jwt-go"
    "github.com/auth0/go-jwt-middleware"

)
 var imagePath = "www.brandt-projects.de/~Eric/OdorImages/"

var Pictures = []Picture{
	Picture{ID:1,Name:imagePath + "rose.jpg"},
	Picture{ID:2,Name:imagePath + "apple.jpeg"},
	Picture{ID:3,Name:imagePath + "soap.jpeg"},
	Picture{ID:4,Name:imagePath + "schoki.jpg"},
}
var	DBCon *sql.DB

func main() {
	
	err := godotenv.Load("config.env")
    Init()
	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With"})
	originsOk := handlers.AllowedOrigins([]string{os.Getenv("ORIGIN_ALLOWED")})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS"})
	r := mux.NewRouter()
	
	if err != nil{
		log.Fatal("Error loading .env file")
	}

	//picture route
	r.Handle("/tests", testHandler)
	r.Handle("/", indexHandler)	
	r.Handle("/bewerte", jwtMiddleware.Handler(bewertungsHandler)).Methods("POST")
    r.Handle("/Bewertung",getBewertung)
	log.Fatal(http.ListenAndServe(":3000", handlers.CORS(headersOk,originsOk,methodsOk)(r)))

}
func Init(){
	db, err := sql.Open(os.Getenv("DB_TYPE"), os.Getenv("DB_CONNECTION_STRING"))
    if err != nil {
        panic(err.Error())  // Just for example purpose. You should use proper error handling instead of panic
    }
    //defer db.Close()
    DBCon = db
}
var testHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {	
	payload, _ := json.Marshal(Pictures)
	w.Write([]byte(payload))
})
var indexHandler  = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {	
	w.Write([]byte("Hello World"))
})

var bewertungsHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	//Get Nickname from Token
    token := r.Header.Get("Authorization")
    token = strings.Split(token, " ")[1]
    nickname := getNick(token)
    //Extract Bewertung from Body
    var b Bewertung
    body, _ := ioutil.ReadAll(r.Body)
    json.Unmarshal(body, &b)
    //DEBUG
    fmt.Println(nickname)
    fmt.Println("Nickname" + b.Nickname)
	//DEBUG
	w.Header().Set("Content-Type", "application/json")
	dbQ := dbQuery{}
	dbQ.AddBewertung(b,DBCon)
	w.Write([]byte(body))
})

var getBewertung = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	//Get Nickname from Token
	dbQ := dbQuery{}
    bew := dbQ.GetAllBewertungen(DBCon)
    str,err := json.Marshal(bew)
    if err != nil{
    	fmt.Println(err)
    }
    w.Write([]byte(str))
})

var jwtMiddleware = jwtmiddleware.New(jwtmiddleware.Options{
  ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
  return []byte(os.Getenv("AUTH0_CLIENT_SECRET")), nil
  },
})


var getNick = func(tokenString string) string{
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
	    // Don't forget to validate the alg is what you expect:
	    if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
	        return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
	    }

	    // hmacSampleSecret is a []byte containing your secret, e.g. []byte("my_secret_key")
	    return []byte(os.Getenv("AUTH0_CLIENT_SECRET")), nil
	})

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
	    str := claims["nickname"].(string)
	    
	    return str
	} else {
	    fmt.Println(err)
	}

	return ""



}
