# API USAGE



## Einloggen und Token anfordern
```
https://twin322.auth0.com/oauth/ro
{
  "client_id": "MXyhz3VEaknbutRPOMxYyJRHApD4Jnhs",
  "username": "s72784@htw-dresden.de",
  "password": "Persiaprince456",
  "connection": "Username-Password-Authentication",
  "scope": "openid nickname"
}
```

## Secret Route der API mit Token anfordern

GET /route   

Authorization bearer <id_token>

# Datenmodell der Datenbank
```
create table Geruch(
    ID int NOT NULL AUTO_INCREMENT,
    NAME varchar(45) NOT NULL,
    URL varchar(80),
    PRIMARY KEY (ID)
);

create table Test(
    TID int NOT NULL AUTO_INCREMENT,
    NAME varchar(45) NOT NULL
    PRIMARY KEY (TID)
);

create table AktivTest(
    TID int NOT NULL,
    DATE varchar(40),
    CONSTRAINT FK_Test FOREIGN KEY (TID)
    REFERENCES Test(TID)
);

create table GeruchTest(
    TID int NOT NULL,
    ID int NOT NULL,
    DATE varchar(40),
    CONSTRAINT FK_Geruch FOREIGN KEY (TID)
    REFERENCES Test(TID),
    CONSTRAINT FK_Test FOREIGN KEY (ID),
    REFERENCES Geruch(ID)
);

create table Bewertung(
    Nickname varchar(50) NOT NULL,
    Bewertung int NOT NULL,
    GID int NOT NULL,
    DATE varchar(50)
);
```

